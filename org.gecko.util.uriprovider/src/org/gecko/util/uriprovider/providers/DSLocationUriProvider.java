/**
 * Copyright (c) 2014 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.uriprovider.providers;

import java.util.Map;

import org.gecko.util.uriprovider.LocationUriProvider;
import org.gecko.util.uriprovider.exceptions.LocationUriException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;

/**
 * Default class that supports OSGi DS using four properties:
 * - 'context' the general identifier for a uri, used in the id field
 * - 'scheme' the location type or schema (e.g. eclipse workspace, file system, database)
 * - 'location' the host name or file volumes
 * - 'path' the path after the schema
 * @author Mark Hoffmann
 * @since 02.09.2014
 */
@Component(name="uriProviderFactory", configurationPolicy = ConfigurationPolicy.REQUIRE)
public class DSLocationUriProvider implements LocationUriProvider {
	
	public static final String ID = "id";
	public static final String SCHEME = "scheme";
	public static final String LOCATION = "location";
	public static final String PATH = "path";
	public static final String CONTEXT = "context";
	
	private String id = null;
	private String scheme = null;
	private String location = null;
	private String path = null;
	private String context = null;
	
	/* 
	 * (non-Javadoc)
	 * @see de.dim.utilities.uriprovider.LocationUriProvider#getId()
	 */
	@Override
	public String getId() {
		if (id != null) {
			return id;
		}
		id = null;
		if (context != null) {
			id = context;
		}
		if (scheme != null) {
			id = id != null ? id + "." + scheme : null;
		}
		if (location != null) {
			String dummyLocationScheme = location;
			if (location.contains(":")) {
				dummyLocationScheme = location.replace(":", "");
			}
			id = id != null ? id + "." + dummyLocationScheme : null;
		}
		return id;
	}
	
	/* 
	 * (non-Javadoc)
	 * @see de.dim.utilities.uriprovider.LocationUriProvider#getLocationUri()
	 */
	@Override
	public String getLocationUri() {
		String uri = null;
		if (scheme != null) {
			uri = scheme + "://";
		}
		if (location != null) {
			uri += uri != null ? location + "/" : "";
		}
		if (path != null) {
			uri += uri != null ? path + "/" : "";
		}
		return uri;
	}
	
	/**
	 * Activate method called by the OSGi framework
	 * @param properties the configuration properties
	 */
	@Activate
	public void activate(Map<String, String> properties) {
		int propertyCount = 0;
		if (properties.containsKey(ID)) {
			id = properties.get(ID);
			propertyCount++;
		}
		if (properties.containsKey(SCHEME)) {
			scheme = properties.get(SCHEME);
			propertyCount++;
		}
		if (properties.containsKey(LOCATION)) {
			location = properties.get(LOCATION);
			propertyCount++;
		}
		if (properties.containsKey(PATH)) {
			path = properties.get(PATH);
			propertyCount++;
		}
		if (properties.containsKey(CONTEXT)) {
			context = properties.get(CONTEXT);
			propertyCount++;
		}
		if (propertyCount < 4 || propertyCount > 5) {
			throw new LocationUriException("At least one property is missing for the DSLocationUriProvider");
		}
	}
	
	/**
	 * Modified method called by the OSGi framework
	 * @param properties the configuration properties
	 */
	@Modified
	public void modified(Map<String, String> properties) {
		if (properties.get(ID) != null) {
			id = properties.get(ID);
		}
		if (properties.get(SCHEME) != null) {
			scheme = properties.get(SCHEME);
		}
		if (properties.get(LOCATION) != null) {
			location = properties.get(LOCATION);
		}
		if (properties.get(PATH) != null) {
			path = properties.get(PATH);
		}
		if (properties.get(CONTEXT) != null) {
			context = properties.get(CONTEXT);
		}
	}

}
