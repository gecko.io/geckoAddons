/**
 * Copyright (c) 2012 - 2016 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 * Customizer that tracks the service providers
 * Each call of add, modify or remove will be counted. Further a {@link CountDownLatch} can be given,
 * to ensure to wait till the service is available
 * @author Mark Hoffmann
 * @since 16.06.2016
 */
public class ServiceProviderCustomizer<S, T>
		implements ServiceTrackerCustomizer<S, T> {
	
	private final AtomicInteger addCount = new AtomicInteger();
	private final AtomicInteger modifyCount = new AtomicInteger();
	private final AtomicInteger removeCount = new AtomicInteger();
	private final BundleContext context;
	private final CountDownLatch createLatch;
	private final CountDownLatch removeLatch;
	private final CountDownLatch modifyLatch;
	
	public ServiceProviderCustomizer(BundleContext context, CountDownLatch createLatch) {
		this.context = context;
		this.createLatch = createLatch;
		this.removeLatch = null;
		this.modifyLatch = null;
	}
	
	public ServiceProviderCustomizer(BundleContext context, CountDownLatch createLatch, CountDownLatch removeLatch) {
		this.context = context;
		this.createLatch = createLatch;
		this.removeLatch = removeLatch;
		this.modifyLatch = null;
	}
	
	public ServiceProviderCustomizer(BundleContext context, CountDownLatch createLatch, CountDownLatch removeLatch, CountDownLatch modifyLatch) {
		this.context = context;
		this.createLatch = createLatch;
		this.removeLatch = removeLatch;
		this.modifyLatch = modifyLatch;
	}
	
	/**
	 * Returns how often add was called
	 * @return how often add was called
	 */
	public int getAddCount() {
		return addCount.get();
	}
	
	/**
	 * Returns how often remove was called
	 * @return how often remove was called
	 */
	public int getRemoveCount() {
		return removeCount.get();
	}
	
	/**
	 * Returns how often modify was called
	 * @return how often modify was called
	 */
	public int getModifyCount() {
		return modifyCount.get();
	}
	
	/* 
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#addingService(org.osgi.framework.ServiceReference)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T addingService(ServiceReference<S> reference) {
		if (createLatch != null) {
			createLatch.countDown();
		}
		addCount.incrementAndGet();
		return (T)context.getService(reference);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#modifiedService(org.osgi.framework.ServiceReference, java.lang.Object)
	 */
	@Override
	public void modifiedService(ServiceReference<S> reference, T service) {
		if (modifyLatch != null) {
			modifyLatch.countDown();
		}
		modifyCount.incrementAndGet();
	}

	/* 
	 * (non-Javadoc)
	 * @see org.osgi.util.tracker.ServiceTrackerCustomizer#removedService(org.osgi.framework.ServiceReference, java.lang.Object)
	 */
	@Override
	public void removedService(ServiceReference<S> reference, T service) {
		if (removeLatch != null) {
			removeLatch.countDown();
		}
		removeCount.incrementAndGet();
	}

}