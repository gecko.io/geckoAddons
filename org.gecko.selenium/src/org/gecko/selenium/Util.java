package org.gecko.selenium;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Util {
	public static List<File> toFiles(String[] strings) {
		if (strings == null) {
			return Collections.emptyList();
		}
		return Arrays.asList(strings).stream().map(File::new).collect(Collectors.toList());
	}

	public static List<String> toList(String[] strings) {

		if (strings == null) {
			return Collections.emptyList();
		}
		return Arrays.asList(strings);
	}

	public static String[] split(final String val) {
		final int pos = val.indexOf('=');
		if (pos == -1) {
			return new String[] { val, "true" };
		}
		return new String[] { val.substring(0, pos), val.substring(pos + 1) };
	}

}
