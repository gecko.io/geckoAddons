///**
// * Copyright (c) 2012 - 2018 Data In Motion and others.
// * All rights reserved. 
// * 
// * This program and the accompanying materials are made available under the terms of the 
// * Eclipse Public License v1.0 which accompanies this distribution, and is available at
// * http://www.eclipse.org/legal/epl-v10.html
// * 
// * Contributors:
// *     Data In Motion - initial API and implementation
// */
//package org.gecko.influxdb.tests;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertTrue;
//
//import java.io.IOException;
//import java.util.Dictionary;
//import java.util.Hashtable;
//import java.util.List;
//import java.util.Map;
//import java.util.Random;
//
//import org.eclipse.emf.ecore.EObject;
//import org.gecko.core.tests.AbstractOSGiTest;
//import org.gecko.core.tests.ServiceChecker;
//import org.gecko.influxdb.api.InfluxDBEntryImpl;
//import org.gecko.influxdb.api.InfluxDBService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.runners.MockitoJUnitRunner;
//import org.osgi.framework.FrameworkUtil;
//import org.osgi.service.cm.Configuration;
//
//import de.dim.traffics.tim.rest.model.tim.TimRestFactory;
//import de.dim.traffics.tim.rest.model.tim.TimRestPackage;
//import de.dim.traffics.tim.rest.model.tim.TrackEntry;
//
///**
// * Integration Tests for the InfluxEObjectMapper
// * 
// * @author ilenia
// * @since May 6, 2019
// */
//@RunWith(MockitoJUnitRunner.class)
//public class InfluxDBEObjMapperIntegrationTest extends AbstractOSGiTest {
//
//	/**
//	 * Creates a new instance.
//	 * @param bundleContext
//	 */
//	public InfluxDBEObjMapperIntegrationTest() {
//		super(FrameworkUtil.getBundle(InfluxDBEObjMapperIntegrationTest.class).getBundleContext());
//	}
//
//	/**
//	 * Here you can put everything you want to be executed before every test
//	 */
//	public void doBefore() {
//
//	}
//
//	/**
//	 * Here you can put everything you want to be executed after every test
//	 */
//	public void doAfter() {
//
//	}
//
//	/**
//	 * Tests the correct implementation of the InfluxEObjectMapper to retrieve data from the influxDB in 
//	 * form of EObject of a given EClass
//	 * @throws IOException
//	 */
//	@Test
//	public void testInfluxDBEObjMapper() throws IOException {
//
//		TrackEntry track = createTrackEntry();
//
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//
//		//		properties.put("protocol", "http");
//		//		properties.put("hostname", "localhost");
//		//		properties.put("port", "8086");
//		//		properties.put("username", "root");
//		//		properties.put("password", "root");
//
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//
//		assertTrue(service.createDB("test"));
//
//		InfluxDBEntryImpl entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("traffic");
//		System.out.println(System.getProperty("java.home"));
//		entry.getFields().put("speed", track.getSpeed());
//		entry.getTags().put("lat", String.valueOf(track.getLat()));
//		entry.getTags().put("lng", String.valueOf(track.getLng()));
//		entry.setTimestamp(track.getTime());
//
//		assertTrue(service.writeSinglePoint("test", entry));
//
//		List<EObject> tracks = service.getEObjectQuery("traffic", "test", null, null, TimRestPackage.Literals.TRACK_ENTRY, null, null);
//		assertFalse(tracks.isEmpty());
//
//		for(EObject obj : tracks) {
//			assertTrue(obj instanceof TrackEntry);
//			TrackEntry t = (TrackEntry) obj;
//			assertEquals(track.getLat(), t.getLat(), 0.01);
//			assertEquals(track.getLng(), t.getLng(), 0.01);
//			assertEquals(track.getSpeed(), t.getSpeed(), 0.01);
//			assertEquals(track.getTime(), t.getTime());
//		}
//
//		assertTrue(service.removeDB("test"));
//	}
//
//	@Test
//	public void testEndDate() throws IOException {
//
//		TrackEntry track = createTrackEntry();
//
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//
//		//		properties.put("protocol", "http");
//		//		properties.put("hostname", "localhost");
//		//		properties.put("port", "8086");
//		//		properties.put("username", "root");
//		//		properties.put("password", "root");
//
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//
//		assertTrue(service.createDB("test"));
//
//		InfluxDBEntryImpl entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("traffic");
//		entry.getFields().put("speed", track.getSpeed());
//		entry.getTags().put("lat", String.valueOf(track.getLat()));
//		entry.getTags().put("lng", String.valueOf(track.getLng()));
//		entry.setTimestamp(track.getTime());
//
//		assertTrue(service.writeSinglePoint("test", entry));
//
//		List<EObject> tracks = service.getEObjectQuery("traffic", "test", null, null, TimRestPackage.Literals.TRACK_ENTRY, null, System.currentTimeMillis());
//		assertFalse(tracks.isEmpty());
//
//		for(EObject obj : tracks) {
//			assertTrue(obj instanceof TrackEntry);
//			TrackEntry t = (TrackEntry) obj;
//			assertEquals(track.getLat(), t.getLat(), 0.01);
//			assertEquals(track.getLng(), t.getLng(), 0.01);
//			assertEquals(track.getSpeed(), t.getSpeed(), 0.01);
//			assertEquals(track.getTime(), t.getTime());
//		}
//
//		assertTrue(service.removeDB("test"));
//	}
//
//	@Test
//	public void testStartDate() throws IOException {
//
//		TrackEntry track = createTrackEntry();
//
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//
//		//		properties.put("protocol", "http");
//		//		properties.put("hostname", "localhost");
//		//		properties.put("port", "8086");
//		//		properties.put("username", "root");
//		//		properties.put("password", "root");
//
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//
//		assertTrue(service.createDB("test"));
//
//		InfluxDBEntryImpl entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("traffic");
//		entry.getFields().put("speed", track.getSpeed());
//		entry.getTags().put("lat", String.valueOf(track.getLat()));
//		entry.getTags().put("lng", String.valueOf(track.getLng()));
//		entry.setTimestamp(track.getTime());
//
//		assertTrue(service.writeSinglePoint("test", entry));
//
//		List<EObject> tracks = service.getEObjectQuery("traffic", "test", null, null, TimRestPackage.Literals.TRACK_ENTRY, System.currentTimeMillis() - 5000, null);
//		assertFalse(tracks.isEmpty());
//
//		for(EObject obj : tracks) {
//			assertTrue(obj instanceof TrackEntry);
//			TrackEntry t = (TrackEntry) obj;
//			assertEquals(track.getLat(), t.getLat(), 0.01);
//			assertEquals(track.getLng(), t.getLng(), 0.01);
//			assertEquals(track.getSpeed(), t.getSpeed(), 0.01);
//			assertEquals(track.getTime(), t.getTime());
//		}
//
//		assertTrue(service.removeDB("test"));
//	}
//
//	@Test
//	public void testOkDate() throws IOException {
//
//		TrackEntry track = createTrackEntry();
//
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//
//		//		properties.put("protocol", "http");
//		//		properties.put("hostname", "localhost");
//		//		properties.put("port", "8086");
//		//		properties.put("username", "root");
//		//		properties.put("password", "root");
//
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//
//		assertTrue(service.createDB("test"));
//
//		InfluxDBEntryImpl entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("traffic");
//		entry.getFields().put("speed", track.getSpeed());
//		entry.getTags().put("lat", String.valueOf(track.getLat()));
//		entry.getTags().put("lng", String.valueOf(track.getLng()));
//		entry.setTimestamp(track.getTime());
//
//		assertTrue(service.writeSinglePoint("test", entry));
//
//		List<EObject> tracks = service.getEObjectQuery("traffic", "test", null, null, TimRestPackage.Literals.TRACK_ENTRY, System.currentTimeMillis() - 5000, System.currentTimeMillis() + 5000);
//		assertFalse(tracks.isEmpty());
//
//		for(EObject obj : tracks) {
//			assertTrue(obj instanceof TrackEntry);
//			TrackEntry t = (TrackEntry) obj;
//			assertEquals(track.getLat(), t.getLat(), 0.01);
//			assertEquals(track.getLng(), t.getLng(), 0.01);
//			assertEquals(track.getSpeed(), t.getSpeed(), 0.01);
//			assertEquals(track.getTime(), t.getTime());
//		}
//
//		assertTrue(service.removeDB("test"));
//	}
//
//	@Test
//	public void testNoPointDate() throws IOException {
//
//		TrackEntry track = createTrackEntry();
//
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//
//		//		properties.put("protocol", "http");
//		//		properties.put("hostname", "localhost");
//		//		properties.put("port", "8086");
//		//		properties.put("username", "root");
//		//		properties.put("password", "root");
//
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//
//		assertTrue(service.createDB("test"));
//
//		InfluxDBEntryImpl entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("traffic");
//		entry.getFields().put("speed", track.getSpeed());
//		entry.getTags().put("lat", String.valueOf(track.getLat()));
//		entry.getTags().put("lng", String.valueOf(track.getLng()));
//		entry.setTimestamp(track.getTime());
//
//		assertTrue(service.writeSinglePoint("test", entry));
//
//		List<EObject> tracks = service.getEObjectQuery("traffic", "test", null, null, TimRestPackage.Literals.TRACK_ENTRY, System.currentTimeMillis() + 10000, System.currentTimeMillis() + 20000);
//		assertTrue(tracks.isEmpty());		
//		assertTrue(service.removeDB("test"));
//	}
//
//	@Test
//	public void testIdTag() throws IOException {
//
//		TrackEntry track = createTrackEntry();
//		String idTagValue = String.valueOf(track.getLat());
//
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//
//		//		properties.put("protocol", "http");
//		//		properties.put("hostname", "localhost");
//		//		properties.put("port", "8086");
//		//		properties.put("username", "root");
//		//		properties.put("password", "root");
//
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//
//		assertTrue(service.createDB("test"));
//
//		InfluxDBEntryImpl entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("traffic");
//		entry.getFields().put("speed", track.getSpeed());
//		entry.getTags().put("lat", String.valueOf(track.getLat()));
//		entry.getTags().put("lng", String.valueOf(track.getLng()));
//		entry.setTimestamp(track.getTime());
//
//		assertTrue(service.writeSinglePoint("test", entry));
//
//		List<EObject> tracks = service.getEObjectQuery("traffic", "test", "lat", idTagValue, TimRestPackage.Literals.TRACK_ENTRY, null, null);
//		assertFalse(tracks.isEmpty());		
//
//		for(EObject obj : tracks) {
//			assertTrue(obj instanceof TrackEntry);
//			TrackEntry t = (TrackEntry) obj;
//			assertEquals(track.getLat(), t.getLat(), 0.01);
//			assertEquals(track.getLng(), t.getLng(), 0.01);
//			assertEquals(track.getSpeed(), t.getSpeed(), 0.01);
//			assertEquals(track.getTime(), t.getTime());
//		}
//
//		assertTrue(service.removeDB("test"));
//	}
//
//	@Test
//	public void testAllTrack() throws IOException {
//
//		TrackEntry track = createTrackEntry();
//
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//
//		//		properties.put("protocol", "http");
//		//		properties.put("hostname", "localhost");
//		//		properties.put("port", "8086");
//		//		properties.put("username", "root");
//		//		properties.put("password", "root");
//
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//
//		assertTrue(service.createDB("test"));
//
//		InfluxDBEntryImpl entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("traffic");
//		entry.getFields().put("speed", track.getSpeed());
//		entry.getTags().put("lat", String.valueOf(track.getLat()));
//		entry.getTags().put("lng", String.valueOf(track.getLng()));
//		entry.setTimestamp(track.getTime());
//
//		assertTrue(service.writeSinglePoint("test", entry));
//
//		Map<String, List<EObject>> resMap = service.getSeriesMap("traffic", "test", "lat", TimRestPackage.Literals.TRACK_ENTRY, null, null);
//		resMap.entrySet().forEach(e->{
//			assertFalse(e.getValue().isEmpty());	
//			for(EObject obj : e.getValue()) {
//				assertTrue(obj instanceof TrackEntry);
//				TrackEntry t = (TrackEntry) obj;
//				assertEquals(track.getLat(), t.getLat(), 0.01);
//				assertEquals(track.getLng(), t.getLng(), 0.01);
//				assertEquals(track.getSpeed(), t.getSpeed(), 0.01);
//				assertEquals(track.getTime(), t.getTime());
//			}
//		});
//
//		assertTrue(service.removeDB("test"));
//	}
//
//	private TrackEntry createTrackEntry() {
//		TrackEntry track = TimRestFactory.eINSTANCE.createTrackEntry();
//
//		double minLat = 45., maxLat = 55., minLng = 9., maxLng = 13., minSp = 0., maxSp = 200.;
//
//		Random r = new Random();
//
//		double lat = minLat + (maxLat - minLat) * r.nextDouble();		
//		track.setLat(lat);
//
//		double lng = minLng + (maxLng - minLng) * r.nextDouble();	
//		track.setLng(lng);
//
//		track.setTime(System.currentTimeMillis());
//
//		double sp = minSp + (maxSp - minSp) * r.nextDouble();	
//		track.setSpeed(sp);
//
//		return track;
//	}
//}
