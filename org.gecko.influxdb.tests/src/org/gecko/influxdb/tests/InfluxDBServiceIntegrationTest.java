///**
// * Copyright (c) 2012 - 2018 Data In Motion and others.
// * All rights reserved. 
// * 
// * This program and the accompanying materials are made available under the terms of the 
// * Eclipse Public License v1.0 which accompanies this distribution, and is available at
// * http://www.eclipse.org/legal/epl-v10.html
// * 
// * Contributors:
// *     Data In Motion - initial API and implementation
// */
//package org.gecko.influxdb.tests;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertTrue;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Dictionary;
//import java.util.HashSet;
//import java.util.Hashtable;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import org.gecko.core.tests.AbstractOSGiTest;
//import org.gecko.core.tests.ServiceChecker;
//import org.gecko.influxdb.api.CSVReader;
//import org.gecko.influxdb.api.InfluxDBEntry;
//import org.gecko.influxdb.api.InfluxDBEntryHelper;
//import org.gecko.influxdb.api.InfluxDBEntryImpl;
//import org.gecko.influxdb.api.InfluxDBService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.runners.MockitoJUnitRunner;
//import org.osgi.framework.FrameworkUtil;
//import org.osgi.service.cm.Configuration;
//
///**
// * Integration Tests for the InfluxDBService 
// * 
// * @author ilenia
// * @since May 6, 2019
// */
//@RunWith(MockitoJUnitRunner.class)
//public class InfluxDBServiceIntegrationTest extends AbstractOSGiTest {
//	
//	/**
//	 * Creates a new instance.
//	 * @param bundleContext
//	 */
//	public InfluxDBServiceIntegrationTest() {
//		super(FrameworkUtil.getBundle(InfluxDBServiceIntegrationTest.class).getBundleContext());
//	}
//
//	/**
//	 * Here you can put everything you want to be exectued before every test
//	 */
//	public void doBefore() {
//		
//	}
//	
//	/**
//	 * Here you can put everything you want to be exectued after every test
//	 */
//	public void doAfter() {
//		
//	}
//	
//	/**
//	 * Tests the DB creation through the InfluxDBService.
//	 * The InfluxDB is installed and running at https://devel.data-in-motion.biz:8286
//	 * @throws IOException
//	 */
//	@Test
//	public void testInfluxDBCreateDB() throws IOException {
//		
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//		
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//		
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//		
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//		
//		assertTrue(service.createDB("test"));
//		assertTrue(service.removeDB("test"));
//	}
//	
//	/**
//	 * Tests the connection to the InfluxDB using username and password from environmental variables
//	 * instead of from configuration.
//	 * @throws IOException
//	 */
//	@Test
//	public void testServiceEnvConfig() throws IOException {
//		System.setProperty("INFLUXDB_USER", "influxdb_admin");
//		System.setProperty("INFLUXDB_PWD", "influxdb_admin_password");
//		
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("username.env", "INFLUXDB_USER");
//		properties.put("password.env", "INFLUXDB_PWD");
//		
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//		
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//		
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//		
//		assertTrue(service.createDB("test"));
//		assertTrue(service.removeDB("test"));
//	}
//	
//	/**
//	 * Tests the connection to the InfluxDB Service when an URL is specified
//	 * @throws IOException
//	 */
//	@Test
//	public void testServiceURLConfig() throws IOException {
//		
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("url", "https://devel.data-in-motion.biz:8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//		
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//		
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//		
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//		
//		assertTrue(service.createDB("test"));
//		assertTrue(service.removeDB("test"));
//	}
//	
//	/**
//	 * Tests the connection to the InfluxDB Service when an URL is specified in an env variable
//	 * @throws IOException
//	 */
//	@Test
//	public void testServiceURLEnvConfig() throws IOException {
//		
//		System.setProperty("INFLUXDB_USER", "influxdb_admin");
//		System.setProperty("INFLUXDB_PWD", "influxdb_admin_password");
//		System.setProperty("INFLUXDB_URL", "https://devel.data-in-motion.biz:8286");
//		
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("username.env", "INFLUXDB_USER");
//		properties.put("password.env", "INFLUXDB_PWD");
//		properties.put("url.env", "INFLUXDB_URL");
//		
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//		
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//		
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//		
//		assertTrue(service.createDB("test"));
//		assertTrue(service.removeDB("test"));		
//	}
//	
//	/**
//	 * Tests the <code>writeSinglePoint()<code> of the InfluxDBService and the corresponding InfluxDBEntry
//	 * @throws IOException 
//	 */
//	@Test
//	public void testWriteSinglePoint() throws IOException {
//	
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//		
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//		
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//		
//		assertTrue(service.createDB("test"));
//		
//		//Setting everything --> true expected
//		InfluxDBEntryImpl entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("system_time");
//		entry.getFields().put("value", 77);
//		entry.getTags().put("policy", "standard");
//		entry.setTimestamp(System.currentTimeMillis());
//		
//		assertTrue(service.writeSinglePoint("test", entry));
//		
//		//Not setting the measurement --> false expected
//		entry = new InfluxDBEntryImpl();
//		entry.getFields().put("value", 77);
//		entry.getTags().put("policy", "standard");
//		entry.setTimestamp(System.currentTimeMillis()+5);
//		
//		assertFalse(service.writeSinglePoint("test", entry));
//		
//		//Not setting at least one field --> false expected
//		entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("system_time");
//		entry.getTags().put("policy", "standard");
//		entry.setTimestamp(System.currentTimeMillis() + 10);
//		
//		assertFalse(service.writeSinglePoint("test", entry));
//		
//		//Setting a measurement which is not a String --> false expected
//		entry = new InfluxDBEntryImpl();
//		entry.setMeasurement(124);
//		entry.getFields().put("value", 77);
//		entry.getTags().put("policy", "standard");
//		entry.setTimestamp(System.currentTimeMillis() + 15);
//		
//		assertFalse(service.writeSinglePoint("test", entry));
//		
//		//Not setting the tags --> true expected (the tags are optional)
//		entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("system_time");
//		entry.getFields().put("value", 77);
//		entry.setTimestamp(System.currentTimeMillis() + 20);
//		
//		assertTrue(service.writeSinglePoint("test", entry));
//		
//		//Setting more than one fields --> true expected
//		entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("system_time");
//		entry.getFields().put("value", 77);
//		entry.getFields().put("value", 46);
//		entry.getTags().put("policy", "standard");
//		entry.setTimestamp(System.currentTimeMillis() + 25);
//		
//		assertTrue(service.writeSinglePoint("test", entry));
//		
//		//Setting more than one tags --> true expected		
//		entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("system_time");
//		entry.getFields().put("value", 46);
//		entry.getTags().put("policy", "standard");
//		entry.getTags().put("buffer", "32");
//		entry.setTimestamp(System.currentTimeMillis() + 30);
//		
//		assertTrue(service.writeSinglePoint("test", entry));		
//		assertTrue(service.removeDB("test"));	
//	}
//	
//	/**
//	 * InfluxDB does not support time shift series, thus we provide a work-around for this issue, in case the user 
//	 * wants to plot two time series, registered at different times, in the same time range. 
//	 * The only thing requested from the user is to save an additional tag with the key <code>timeShiftTag<code> 
//	 * to the point whose timestamp is to be used later as time shift for other time series. 
//	 * Here we tests the method to retrieve this tag back.
//	 * @throws IOException 
//	 */
//	@Test
//	public void testTimeShift() throws IOException {
//		
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//		
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//		
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//		
//		assertTrue(service.createDB("test"));
//		
//		//Setting everything --> true expected
//		InfluxDBEntryImpl entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("system_time");
//		entry.getFields().put("value", 77);
//		entry.getTags().put("policy", "standard");
//		entry.setTimestamp(System.currentTimeMillis());
//		
//		assertTrue(service.writeSinglePoint("test", entry));
//		
//		entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("system_time");
//		entry.getFields().put("value", 88);
//		entry.getTags().put("policy", "standard");
//		long ts = System.currentTimeMillis();
//		entry.getTags().put("timeShift", String.valueOf(ts));
//		entry.setTimestamp(System.currentTimeMillis() + 5);
//		
//		assertTrue(service.writeSinglePoint("test", entry));
//		
//		long timeShift = service.getTimeShift("test", "timeShift");
//		assertTrue(timeShift != 0);
//		assertEquals(ts, timeShift);
//		
//		assertTrue(service.removeDB("test"));	
//	}
//	
//	/**
//	 * Tests the <code>writeTimeSeries()<code> of the InfluxDBService and the corresponding InfluxDBEntry
//	 * @throws IOException 
//	 */
//	@Test
//	public void testWriteTimeSeries() throws IOException {
//		
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//		
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//		
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//		
//		assertTrue(service.createDB("test"));		
//		
//		InfluxDBEntryImpl entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("system_time");
//		entry.getFields().put("value", 77);
//		entry.getTags().put("policy", "standard");
//		entry.setTimestamp(System.currentTimeMillis());
//		
//		InfluxDBEntryImpl entry2 = new InfluxDBEntryImpl();
//		entry2.setMeasurement("system_time");
//		entry2.getFields().put("value", 88);
//		entry2.getTags().put("policy", "standard");
//		entry2.setTimestamp(System.currentTimeMillis()+5);
//		
//		List<InfluxDBEntry> entries = new ArrayList<InfluxDBEntry>();
//		entries.add(entry);
//		entries.add(entry2);
//		
//		assertTrue(service.writeTimeSeries("test", entries));
//		assertTrue(service.removeDB("test"));	
//	}
//	
//	/**
//	 * Tests the <code>writeWithTimeShift()<code> of the InfluxDBService and the corresponding InfluxDBEntry
//	 * @throws IOException 
//	 */
//	@Test
//	public void testWriteWithTimeShift() throws IOException {
//		
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//		
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//		
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//		
//		assertTrue(service.createDB("test"));
//		
//		//Setting everything --> true expected
//		InfluxDBEntryImpl entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("system_time");
//		entry.getFields().put("value", 44);
//		entry.getTags().put("policy", "standard");
//		entry.setTimestamp(System.currentTimeMillis());
//		
//		assertTrue(service.writeSinglePoint("test", entry));
//		
//		entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("system_time");
//		entry.getFields().put("value", 55);
//		entry.getTags().put("policy", "standard");
//		long ts = System.currentTimeMillis();
//		entry.getTags().put("timeShift", String.valueOf(ts));
//		entry.setTimestamp(System.currentTimeMillis() + 5);
//		
//		assertTrue(service.writeSinglePoint("test", entry));
//		
//		long timeShift = service.getTimeShift("test", "timeShift");
//		assertTrue(timeShift != 0);
//		assertEquals(ts, timeShift);
//		
//		entry = new InfluxDBEntryImpl();
//		entry.setMeasurement("system_time");
//		entry.getFields().put("value", 77);
//		entry.getTags().put("policy", "standard");
//		entry.setTimestamp(System.currentTimeMillis());
//		
//		InfluxDBEntryImpl entry2 = new InfluxDBEntryImpl();
//		entry2.setMeasurement("system_time");
//		entry2.getFields().put("value", 88);
//		entry2.getTags().put("policy", "standard");
//		entry2.setTimestamp(System.currentTimeMillis()+1000);
//		
//		List<InfluxDBEntry> entries = new ArrayList<InfluxDBEntry>();
//		entries.add(entry);
//		entries.add(entry2);
//		
//		assertTrue(service.writeWithTimeShift("test", entries, timeShift));
//		assertTrue(service.removeDB("test"));	
//	}
//	
//	/**
//	 * Writes points starting from a CSV file and using the CSVReader to convert it into a more suitable format for
//	 * the InfluxDBService. In this case all the columns of the CSV file will be written in the DB as String values.
//	 * @throws IOException 
//	 */
//	@Test
//	public void testWriteFromCSVConverter() throws IOException {
//		
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//		
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//		
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//		
//		ServiceChecker<?> readerChecker = createCheckerTrackedForCleanUp(CSVReader.class);
//		readerChecker.start();
//		assertNotNull(readerChecker);
//		assertEquals(1, readerChecker.getCurrentCreateCount(true));
//		
//		CSVReader CSVReader = getService(CSVReader.class);
//		assertNotNull(CSVReader);
//		
//		String filePath = "data/2019-05-12T13-31-04.csv";
//		Map<String, List<Object>> result = CSVReader.convert(filePath, ",", true);
//		
//		int size = 854;
//		result.values().forEach(l->{
//			assertEquals(size, l.size());
//		});
//		
//		assertTrue(service.createDB("test"));
//		
//		InfluxDBEntryHelper entryHelper = getService(InfluxDBEntryHelper.class);
//		assertNotNull(entryHelper);	
//		
//		Set<String> fieldKeys = new HashSet<String>();
//		fieldKeys.add("speed in km/h");
//	
//		Set<String> tagsKeys = new HashSet<String>();
//		tagsKeys.add("latitude");
//		tagsKeys.add("longitude");
//		
//		String timeKey = "timestamp";
//		
//		entryHelper.initialize("traffic", fieldKeys, tagsKeys, timeKey);
//		List<InfluxDBEntry> entries = entryHelper.getInfluxdbEntries(result);
//		
//		assertTrue(service.writeTimeSeries("test", entries));
//		assertTrue(service.removeDB("test"));		
//	}
//	
//	/**
//	 * Writes points starting from a CSV file and using the CSVReader to convert it into a more suitable format for
//	 * the InfluxDBService. In this case we are setting the types options in the CSV reader, so we can specify of which 
//	 * data type the columns are. In such a way, the data will be saved in the DB with the corresponding type.
//	 * @throws IOException
//	 */
//	@Test
//	public void testWriteFromCSVConverterOptions() throws IOException {
//		
//		Dictionary<String, Object> properties = new Hashtable<String, Object>();
//		properties.put("protocol", "https");
//		properties.put("hostname", "devel.data-in-motion.biz");
//		properties.put("port", "8286");
//		properties.put("username", "influxdb_admin");
//		properties.put("password", "influxdb_admin_password");
//		
//		Configuration configuration = createConfigForCleanup("InfluxDBClient", "test", "?", properties);
//		ServiceChecker<?> checker = getServiceCheckerForConfiguration(configuration);
//		assertNotNull(checker);
//		assertEquals(1, checker.getCurrentCreateCount(true));
//		
//		InfluxDBService service = getService(InfluxDBService.class);
//		assertNotNull(service);
//		
//		ServiceChecker<?> readerChecker = createCheckerTrackedForCleanUp(CSVReader.class);
//		readerChecker.start();
//		assertNotNull(readerChecker);
//		assertEquals(1, readerChecker.getCurrentCreateCount(true));
//		
//		CSVReader reader = getService(CSVReader.class);
//		assertNotNull(reader);
//		
//		String filePath = "data/2019-05-12T13-31-04.csv";
//		Map<String, Object> options = CSVReader.createDefaultOptions(",", true);
//		Class<?>[] types = {Long.TYPE, Integer.TYPE, String.class, String.class};
//		options.put(CSVReader.PROP_TYPE_ARRAY, types);
//		
//		Map<String, List<Object>> result = reader.convert(filePath, options);
//		
//		int size = 854;
//		result.values().forEach(l->{
//			assertEquals(size, l.size());
//		});
//		
//		assertTrue(service.createDB("test"));
//		
//		ServiceChecker<?> entryChecker = createCheckerTrackedForCleanUp(InfluxDBEntryHelper.class);
//		entryChecker.start();
//		assertNotNull(entryChecker);
//		assertEquals(1, entryChecker.getCurrentCreateCount(true));
//		
//		InfluxDBEntryHelper entryHelper = getService(InfluxDBEntryHelper.class);
//		assertNotNull(entryHelper);	
//		
//		Set<String> fieldKeys = new HashSet<String>();
//		fieldKeys.add("speed in km/h");
//	
//		Set<String> tagsKeys = new HashSet<String>();
//		tagsKeys.add("latitude");
//		tagsKeys.add("longitude");
//		
//		String timeKey = "timestamp";
//		
//		entryHelper.initialize("traffic", fieldKeys, tagsKeys, timeKey);
//		List<InfluxDBEntry> entries = entryHelper.getInfluxdbEntries(result);
//		assertTrue(service.writeTimeSeries("test", entries));	
//		assertTrue(service.removeDB("test"));		
//	}
//	
//	
//	
//}
