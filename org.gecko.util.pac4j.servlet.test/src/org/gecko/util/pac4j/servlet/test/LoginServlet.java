/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.pac4j.servlet.test;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.http.HttpMethod;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.http.whiteboard.propertytypes.HttpWhiteboardServletPattern;
import org.osgi.service.http.whiteboard.propertytypes.HttpWhiteboardTarget;
import org.pac4j.core.context.Pac4jConstants;
import org.pac4j.oidc.profile.OidcProfile;

/**
 * 
 * @author jalbert
 * @since 5 Dec 2018
 */
@Component(
	service = Servlet.class
)
@HttpWhiteboardServletPattern("/login/*")
@HttpWhiteboardTarget("(id=login)")
public class LoginServlet extends HttpServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = 4617795115021768822L;

	/* 
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Map<String, Object> profiles = (Map<String, Object>) req.getSession().getAttribute(Pac4jConstants.USER_PROFILES);
		OidcProfile profile = (OidcProfile) profiles.get("ui");
		
		HttpClient client = new HttpClient();
		try {
			client.start();
			Request request = client.newRequest("http://localhost:8081/bearer/test");
			request.method(HttpMethod.GET);
			request.header("Authorization", "Bearer " + profile.getIdTokenString());
			ContentResponse contentResponse = request.send();
			String response = "Remote response: " + contentResponse.getContentAsString();
			resp.getOutputStream().write(response.getBytes());
		} catch (Exception e) {
			
		}
		
	}
	
}
