/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.pac4j.servlet.test;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.http.whiteboard.propertytypes.HttpWhiteboardServletPattern;
import org.osgi.service.http.whiteboard.propertytypes.HttpWhiteboardTarget;
import org.pac4j.core.context.Pac4jConstants;
import org.pac4j.core.profile.CommonProfile;

/**
 * 
 * @author jalbert
 * @since 5 Dec 2018
 */
@Component(
	service = Servlet.class
)
@HttpWhiteboardServletPattern("/bearer/*")
@HttpWhiteboardTarget("(id=bearer)")
public class BearerServlet extends HttpServlet {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/* 
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		@SuppressWarnings("unchecked")
		Map<String, Object> profiles = (Map<String, Object>) req.getAttribute(Pac4jConstants.USER_PROFILES);
		CommonProfile profile = (CommonProfile) profiles.get("bearer");
		String name = profile.getFirstName() + " " + profile.getFamilyName();
		resp.getOutputStream().write(name.getBytes());
	}
	
}
