/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.pac4j.clients;

import java.util.Objects;

import org.gecko.util.pac4j.clients.config.KeycloakOidcClientConfig;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;
import org.pac4j.core.client.Client;
import org.pac4j.oidc.config.KeycloakOidcConfiguration;

import com.nimbusds.jose.JWSAlgorithm;

/**
 * The {@link KeycloackOidClient} is an OID Client, performing a OIDC login action if necessary.
 * @author Juergen Albert
 * @since 10 Sep 2018
 */
@Component(service = Client.class, name = "KeycloackOidcClient", configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = KeycloakOidcClientConfig.class)
public class KeycloackOidcClient extends org.pac4j.oidc.client.KeycloakOidcClient{

	@Activate
	public void activate(KeycloakOidcClientConfig clientConfig) throws ConfigurationException {
		
		KeycloakOidcConfiguration config = new KeycloakOidcConfiguration();
		config.setBaseUri(clientConfig.oid_baseUri());
		config.setRealm(clientConfig.oid_realm());
		config.setClientId(clientConfig.oid_clientId());
		config.setSecret(clientConfig.oid_secret());
		setName(constructClientName(clientConfig));
		JWSAlgorithm jwsAlgorithm = JWSAlgorithm.parse(clientConfig.oid_jws_algorithm().toString());
		config.setPreferredJwsAlgorithm(jwsAlgorithm);
		setCallbackUrl(clientConfig.client_callbackUrl());
		config.setExpireSessionWithToken(true);
		setConfiguration(config);
	}

	protected String constructClientName(KeycloakOidcClientConfig config) {
		if (Objects.nonNull(config.pac4j_clientId()) && !config.pac4j_clientId().isBlank()) {
			return config.pac4j_clientId();
		} else {
			return config.oid_clientId();
		}
	}	
}
