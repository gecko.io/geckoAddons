/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.pac4j.clients.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition
public @interface KeycloakOidcClientConfig {
	@AttributeDefinition(description = "The base URI of the OIC Server auth entpoint (e.g. https://localhost:8080/auth for Keycloak)")
	String oid_baseUri();
	String oid_realm();
	String oid_clientId();
	String oid_secret();
	String client_id();
	String pac4j_clientId();
	
	JWSAlgorithms oid_jws_algorithm() default JWSAlgorithms.RS256;

	/**
	 * @return
	 */
	String client_callbackUrl();
}