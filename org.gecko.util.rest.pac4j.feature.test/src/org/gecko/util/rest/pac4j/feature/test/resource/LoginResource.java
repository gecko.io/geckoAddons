/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.rest.pac4j.feature.test.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.gecko.util.rest.pac4j.feature.test.annotation.RequireRuntime;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsResource;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsWhiteboardTarget;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.jax.rs.annotations.Pac4JCallback;
import org.pac4j.jax.rs.annotations.Pac4JProfile;
import org.pac4j.jax.rs.annotations.Pac4JSecurity;
import org.pac4j.oidc.profile.OidcProfile;

/**
 * 
 * @author jalbert
 * @since 31 Aug 2018
 */
@RequireRuntime
@Component(service = LoginResource.class, scope = ServiceScope.PROTOTYPE)
@JaxrsResource
@JaxrsWhiteboardTarget("(jersey.jaxrs.whiteboard.name=login)")
//@JaxrsApplicationSelect("(osgi.jaxrs.application.base=*)")
@Consumes(MediaType.WILDCARD)
@Produces(MediaType.WILDCARD)
@Path("/")
public class LoginResource {
	
	@Reference
	ClientBuilder clientBuilder;
	
	@GET
	@Pac4JCallback()
	@Path("callback")
	public Response callback () {
		return Response.ok("Thx").build();
	}

	@GET
	@Path("login")
	@Pac4JSecurity(clients = "ui")
	public Response login(@Pac4JProfile CommonProfile profile) {
		return Response.ok("Welcome " + profile.getFirstName() + " " + profile.getFamilyName()).build();
	}

	@GET
	@Path("remote")
	@Pac4JSecurity(clients = "ui")
	public Response remote(@Pac4JProfile CommonProfile profile) {
		OidcProfile oidProfile = (OidcProfile) profile;
		Invocation invocation = clientBuilder.build().target("http://localhost:8186/bearer/remote")
		.request(MediaType.WILDCARD)
		.accept(MediaType.TEXT_PLAIN)
		.header("Authorization", "Bearer " + oidProfile.getIdTokenString())
		.buildGet();

		System.err.println(oidProfile.getAccessToken().toAuthorizationHeader());
		
		System.err.println(oidProfile.getIdTokenString());
		
		Response response = invocation.invoke();
		
		String readEntity = response.readEntity(String.class);
		
		return Response.ok("Remote returned " + readEntity).build();
	}
}
