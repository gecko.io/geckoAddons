/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	   Jerome Leleu- initial API and implementation  
 *     Data In Motion - modified it for OSGi environements
 */
package org.gecko.util.pac4j.servlet;

import static org.pac4j.core.util.CommonHelper.assertNotNull;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.http.whiteboard.propertytypes.HttpWhiteboardFilterPattern;
import org.pac4j.core.client.Client;
import org.pac4j.core.config.Config;
import org.pac4j.core.context.J2EContext;
import org.pac4j.core.context.Pac4jConstants;
import org.pac4j.core.context.session.J2ESessionStore;
import org.pac4j.core.engine.CallbackLogic;
import org.pac4j.core.engine.DefaultCallbackLogic;
import org.pac4j.core.http.adapter.J2ENopHttpActionAdapter;

/**
 * <p>This filter finishes the login process for an indirect client, based on the {@link #callbackLogic}.</p>
 *
 * <p>The configuration can be provided via servlet parameters, setters or constructors for the following options:</p>
 * <ul>
 *     <li><code>configFactory</code> (the class name of the factory to build the configuration) or <code>config</code> (the configuration itself)</li>
 *     <li><code>defaultUrl</code> (default url after login if none was requested)</li>
 *     <li><code>multiProfile</code> (whether multiple profiles should be kept)</li>
 *     <li><code>renewSession</code> (whether the session must be renewed after login)</li>
 * </ul>
 *
 * @author Jerome Leleu
 * @since 1.0.0
 */
@Component(
		name = "Pac4jCallbackFilter",
		service = Filter.class,
		configurationPolicy = ConfigurationPolicy.REQUIRE
	)
@HttpWhiteboardFilterPattern("/callback")
public class CallbackFilter extends AbstractConfigFilter {

    private CallbackLogic<Object, J2EContext> callbackLogic = new DefaultCallbackLogic<>();

    private String defaultUrl;

    private Boolean multiProfile;

    private Boolean renewSession;

    private Boolean saveInSession;
    
    @SuppressWarnings("rawtypes")
   	@Reference(name = "client", cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
   	private Client client;

    public CallbackFilter() {}

    public CallbackFilter(final Config config) {
        setConfig(config);
    }

    public CallbackFilter(final Config config, final String defaultUrl) {
        this(config);
        this.defaultUrl = defaultUrl;
    }

    @SuppressWarnings("rawtypes")
   	@Activate
    public void activate(Map<String, Object> properties) {
		Config rsConfig = new Config(client);
		rsConfig.setSessionStore(new J2ESessionStore());
		rsConfig.setCallbackLogic(new DefaultCallbackLogic());
		setConfig(rsConfig);
		this.defaultUrl = getStringParam(properties, Pac4jConstants.DEFAULT_URL, this.defaultUrl);
        this.multiProfile = getBooleanParam(properties, Pac4jConstants.MULTI_PROFILE, this.multiProfile);
        this.renewSession = getBooleanParam(properties, Pac4jConstants.RENEW_SESSION, this.renewSession);
    }
    
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);

        this.defaultUrl = getStringParam(filterConfig, Pac4jConstants.DEFAULT_URL, this.defaultUrl);
        this.multiProfile = getBooleanParam(filterConfig, Pac4jConstants.MULTI_PROFILE, this.multiProfile);
        this.renewSession = getBooleanParam(filterConfig, Pac4jConstants.RENEW_SESSION, this.renewSession);

        // check backward incompatibility
        checkForbiddenParameter(filterConfig, "clientsFactory");
    }

    @SuppressWarnings("unchecked")
	@Override
    protected void internalFilter(final HttpServletRequest request, final HttpServletResponse response,
                                           final FilterChain chain) throws IOException, ServletException {

        assertNotNull("callbackLogic", callbackLogic);

        final Config config = getConfig();
        assertNotNull("config", config);
        final J2EContext context = new J2EContext(request, response, config.getSessionStore());

        callbackLogic.perform(context, config, J2ENopHttpActionAdapter.INSTANCE, this.defaultUrl, this.saveInSession, this.multiProfile, this.renewSession, client.getName());
    }

    public String getDefaultUrl() {
        return this.defaultUrl;
    }

    public void setDefaultUrl(final String defaultUrl) {
        this.defaultUrl = defaultUrl;
    }

    public Boolean getMultiProfile() {
        return multiProfile;
    }

    public void setMultiProfile(final Boolean multiProfile) {
        this.multiProfile = multiProfile;
    }

    public Boolean getRenewSession() {
        return renewSession;
    }

    public void setRenewSession(final Boolean renewSession) {
        this.renewSession = renewSession;
    }

    public CallbackLogic<Object, J2EContext> getCallbackLogic() {
        return callbackLogic;
    }

    public void setCallbackLogic(final CallbackLogic<Object, J2EContext> callbackLogic) {
        this.callbackLogic = callbackLogic;
    }
}