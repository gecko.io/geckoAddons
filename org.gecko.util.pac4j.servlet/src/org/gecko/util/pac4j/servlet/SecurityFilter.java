/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 * 	   Jerome Leleu, Michael Remond - initial API and implementation  
 *     Data In Motion - modified it for OSGi environements
 */
package org.gecko.util.pac4j.servlet;

import static org.pac4j.core.util.CommonHelper.assertNotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;
import org.osgi.service.http.whiteboard.propertytypes.HttpWhiteboardFilterPattern;
import org.pac4j.core.authorization.authorizer.Authorizer;
import org.pac4j.core.client.Client;
import org.pac4j.core.config.Config;
import org.pac4j.core.context.J2EContext;
import org.pac4j.core.context.Pac4jConstants;
import org.pac4j.core.context.session.J2ESessionStore;
import org.pac4j.core.engine.DefaultCallbackLogic;
import org.pac4j.core.engine.DefaultSecurityLogic;
import org.pac4j.core.engine.SecurityLogic;
import org.pac4j.core.exception.TechnicalException;
import org.pac4j.core.profile.CommonProfile;

import com.nimbusds.jwt.proc.BadJWTException;

/**
 * <p>This filter protects an url, based on the {@link #securityLogic}.</p>
 *
 * <p>The configuration can be provided via servlet parameters, setters, constructors or via Service properties for the following options:</p>
 * <ul>
 *     <li><code>configFactory</code> (the class name of the factory to build the configuration) or <code>config</code> (the configuration itself)</li>
 *     <li><code>clients</code> (list of clients for authentication)</li>
 *     <li><code>authorizers</code> (list of authorizers)</li>
 *     <li><code>matchers</code> (list of matchers)</li>
 *     <li><code>multiProfile</code>  (whether multiple profiles should be kept).</li>
 * </ul>
 *
 * @author Juergen Albert, Jerome Leleu, Michael Remond
 * @since 1.0.0
 */
@Component(
	name = "Pac4jSecurityFilter",
	service = Filter.class,
	configurationPolicy = ConfigurationPolicy.REQUIRE
)
@HttpWhiteboardFilterPattern("/*")
public class SecurityFilter extends AbstractConfigFilter {

    private SecurityLogic<Object, J2EContext> securityLogic = new DefaultSecurityLogic<>();

    private String clients;

    private String authorizers;

    private String matchers;

    private Boolean multiProfile;
    
    @SuppressWarnings("rawtypes")
	@Reference(name = "clients", cardinality = ReferenceCardinality.AT_LEAST_ONE, policy = ReferencePolicy.STATIC, policyOption = ReferencePolicyOption.GREEDY)
	private volatile List<Client> clientInstances = new LinkedList<>();

	private Map<String, Authorizer<? extends CommonProfile>> authorizerInstance = new HashMap<>();

    public SecurityFilter() {}

    public SecurityFilter(final Config config) {
        setConfig(config);
    }

    public SecurityFilter(final Config config, final String clients) {
        this(config);
        this.clients = clients;
    }

    public SecurityFilter(final Config config, final String clients, final String authorizers) {
        this(config, clients);
        this.authorizers = authorizers;
    }

    @SuppressWarnings("rawtypes")
	@Activate
    public void activate(Map<String, Object> properties) {
    	StringJoiner joiner = new StringJoiner(",");
    	clientInstances.stream().map(c -> c.getName()).forEach(joiner::add);
    	Config rsConfig = new Config(clientInstances);
		rsConfig.setSessionStore(new J2ESessionStore());
		rsConfig.setCallbackLogic(new DefaultCallbackLogic());
		authorizerInstance.forEach(rsConfig::addAuthorizer);
		setConfig(rsConfig);
		this.authorizers = getStringParam(properties, Pac4jConstants.AUTHORIZERS, this.authorizers);
    	this.clients = joiner.toString();
        this.matchers = getStringParam(properties, Pac4jConstants.MATCHERS, this.matchers);
        this.multiProfile = getBooleanParam(properties, Pac4jConstants.MULTI_PROFILE, this.multiProfile);
    }
    
	/* Sets the authorizerInstances.
	 * @param authorizerInstances the authorizerInstances to set
	 */
    @Reference(name="authorizer", cardinality=ReferenceCardinality.MULTIPLE, policy=ReferencePolicy.STATIC, policyOption=ReferencePolicyOption.GREEDY)
	public void setAuthorizerInstances(Authorizer<? extends CommonProfile> authorizer, Map<String, Object> options) {
		String name = (String) options.get("name");
		if(name ==  null) {
			name = authorizer.getClass().getSimpleName();
		}
		authorizerInstance.put(name, authorizer);
	}
    
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);

        this.clients = getStringParam(filterConfig, Pac4jConstants.CLIENTS, this.clients);
        this.authorizers = getStringParam(filterConfig, Pac4jConstants.AUTHORIZERS, this.authorizers);
        this.matchers = getStringParam(filterConfig, Pac4jConstants.MATCHERS, this.matchers);
        this.multiProfile = getBooleanParam(filterConfig, Pac4jConstants.MULTI_PROFILE, this.multiProfile);

        // check backward incompatibility
        checkForbiddenParameter(filterConfig, "clientsFactory");
        checkForbiddenParameter(filterConfig, "isAjax");
        checkForbiddenParameter(filterConfig, "stateless");
        checkForbiddenParameter(filterConfig, "requireAnyRole");
        checkForbiddenParameter(filterConfig, "requireAllRoles");
        checkForbiddenParameter(filterConfig, "clientName");
        checkForbiddenParameter(filterConfig, "authorizerName");
        checkForbiddenParameter(filterConfig, "matcherName");
    }

    /* 
     * (non-Javadoc)
     * @see org.gecko.util.pac4j.servlet.AbstractConfigFilter#internalFilter(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)
     */
    @SuppressWarnings("unchecked")
	@Override
    protected final void internalFilter(final HttpServletRequest request, final HttpServletResponse response,
                                        final FilterChain filterChain) throws IOException, ServletException {

        assertNotNull("securityLogic", securityLogic);

        final Config config = getConfig();
        assertNotNull("config", config);
        final J2EContext context = new J2EContext(request, response, config.getSessionStore());
        try {
        	securityLogic.perform(context, config, (ctx, profiles, parameters) -> {
        		
        		filterChain.doFilter(request, response);
        		return null;
        		
        	}, org.pac4j.core.http.adapter.J2ENopHttpActionAdapter.INSTANCE, clients, authorizers, matchers, multiProfile);
        } catch (TechnicalException e) {
			if(e.getCause() instanceof BadJWTException) {
				response.setStatus(401);
				response.getOutputStream().write(e.getCause().getMessage().getBytes());
			} else {
				throw e;
			}
		}
    }

    public String getClients() {
        return clients;
    }

    public void setClients(final String clients) {
        this.clients = clients;
    }

    public String getAuthorizers() {
        return authorizers;
    }

    public void setAuthorizers(final String authorizers) {
        this.authorizers = authorizers;
    }

    public String getMatchers() {
        return matchers;
    }

    public void setMatchers(final String matchers) {
        this.matchers = matchers;
    }

    public Boolean getMultiProfile() {
        return multiProfile;
    }

    public void setMultiProfile(final Boolean multiProfile) {
        this.multiProfile = multiProfile;
    }

    public SecurityLogic<Object, J2EContext> getSecurityLogic() {
        return securityLogic;
    }

    public void setSecurityLogic(final SecurityLogic<Object, J2EContext> securityLogic) {
        this.securityLogic = securityLogic;
    }
}
