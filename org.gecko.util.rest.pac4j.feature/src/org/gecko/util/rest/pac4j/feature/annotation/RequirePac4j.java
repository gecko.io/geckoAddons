/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.rest.pac4j.feature.annotation;

import static java.lang.annotation.ElementType.PACKAGE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.gecko.util.rest.pac4j.feature.constants.GeckoPac4JConstants;
import org.osgi.annotation.bundle.Requirement;

@Documented
@Retention(CLASS)
@Target({ TYPE, PACKAGE })
/**
 * Requries The Pac4J Feature 
 * @author Juergen Albert
 * @since 9 Sep 2018
 */
@Requirement(namespace = GeckoPac4JConstants.NS, filter="(" + GeckoPac4JConstants.NS + "=" + GeckoPac4JConstants.NAME + ")")
public @interface RequirePac4j {

}
