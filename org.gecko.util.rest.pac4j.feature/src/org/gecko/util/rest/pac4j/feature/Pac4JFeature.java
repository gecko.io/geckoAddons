/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.util.rest.pac4j.feature;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;

import org.gecko.util.rest.pac4j.feature.constants.GeckoPac4JConstants;
import org.osgi.annotation.bundle.Capability;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsExtension;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsName;
import org.pac4j.core.client.Client;
import org.pac4j.core.config.Config;
import org.pac4j.core.engine.DefaultCallbackLogic;
import org.pac4j.jax.rs.features.JaxRsConfigProvider;
import org.pac4j.jax.rs.features.Pac4JSecurityFeature;
import org.pac4j.jax.rs.jersey.features.Pac4JValueFactoryProvider;
import org.pac4j.jax.rs.servlet.features.ServletJaxRsContextFactoryProvider;
import org.pac4j.jax.rs.servlet.pac4j.ServletSessionStore;

/**
 * The configurable security feature for pac4j
 * @author Juergen Albert
 * @since 31 Aug 2018
 */
@Component(name = "Pac4JFeature", scope = ServiceScope.PROTOTYPE, configurationPolicy = ConfigurationPolicy.REQUIRE)
@JaxrsExtension
@JaxrsName("Pac4JFeature")
@Capability(namespace = GeckoPac4JConstants.NS, name = GeckoPac4JConstants.NAME)
public class Pac4JFeature implements Feature{

	private Config rsConfig;
	
	@SuppressWarnings("rawtypes")
	@Reference(name = "clients", cardinality = ReferenceCardinality.AT_LEAST_ONE, policy = ReferencePolicy.STATIC, policyOption = ReferencePolicyOption.GREEDY)
	volatile List<Client> clients = new LinkedList<>();
	
	@SuppressWarnings("rawtypes")
	@Activate
	public void activate(Map<String, Object> properties) {
		rsConfig = new Config(clients);
		rsConfig.setSessionStore(new ServletSessionStore());
		rsConfig.setCallbackLogic(new DefaultCallbackLogic());
	}
	
	/* 
	 * (non-Javadoc)
	 * @see javax.ws.rs.core.Feature#configure(javax.ws.rs.core.FeatureContext)
	 */
	@Override
	public boolean configure(FeatureContext context) {
		context.register(new JaxRsConfigProvider(rsConfig));
		context.register(new ServletJaxRsContextFactoryProvider());
		context.register(new Pac4JSecurityFeature());
		context.register(new Pac4JValueFactoryProvider.Binder());
		return true;
	}

}
